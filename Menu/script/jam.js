const bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
const seminggu = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];
const totalBulan = [28, 30, 31];

const kalender = document.querySelector('.kalender');
const waktu = document.querySelector('.waktu p')
const tanggalBox = document.querySelector('.tanggal-box');
const dataBulan = document.getElementById('bulan')
const dataHari = document.getElementById('hari')

const bulanIni = new Date().getMonth();
const tanggalHariIni = new Date().getDate();
const hariIni = new Date().getDay();


function controlCalender() {
    const tanggalSebulan = (bulanIni % 2 == 1 ? totalBulan[2] : totalBulan[1]);
    for (var start = 1; start <= tanggalSebulan; start++) {
        tanggalBox.innerHTML += `<span class="tanggal">${start}</span>`
    }
}

function controlDayMonth() {
    dataBulan.innerHTML = bulan[bulanIni];
    dataHari.innerHTML = seminggu[hariIni];
}

function controlDate() {
    const seluruhTanggal = document.querySelectorAll('.tanggal-box span');

    seluruhTanggal[tanggalHariIni - 1].style.color = 'red'
    seluruhTanggal[tanggalHariIni - 1].style.fontWeight = 'bold'
    seluruhTanggal[tanggalHariIni - 1].style.borderBottom = '3px solid red'
}


function constrolTime() {
    setInterval(() => {
        const jam = new Date().getHours();
        const menit = new Date().getMinutes();
        const detik = new Date().getSeconds();
        jam
        waktu.innerHTML = jam + " : " + menit + " : " + detik;
    }, 1000);
}






controlCalender()
controlDayMonth()
controlDate()
constrolTime()
