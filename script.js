document.addEventListener("scroll", (e) => {
    if (e.path[1].scrollY > 0) {
        document.querySelector("nav").classList.add('navbar-scroll')
    } else {
        document.querySelector("nav").classList.remove('navbar-scroll')
    }
})

